"use strict";

import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc.js';
import timezone from 'dayjs/plugin/timezone.js';

const now = new Date();

dayjs.extend(utc);
dayjs.extend(timezone);

console.log( "dayjs : ", dayjs( now ));
