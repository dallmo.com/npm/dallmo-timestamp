// all are required for timezone adjustment below

const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc.js");
const timezone = require("dayjs/plugin/timezone.js");

      dayjs.extend(utc);
      dayjs.extend(timezone);

/*
  ref : 
    display format : https://day.js.org/docs/en/display/format
*/

///////////////////////////////////////////////////////////////////////
function dallmo_timestamp(){
  
  let full, YY, YYYY, MM, DD, HH, mm, ss, sss, ms, date, time;

  try {
  
    const now = new Date();
          YY   = dayjs( now ).tz("Asia/Hong_Kong").format("YY");
          YYYY = dayjs( now ).tz("Asia/Hong_Kong").format("YYYY");
          MM   = dayjs( now ).tz("Asia/Hong_Kong").format("MM");
          DD   = dayjs( now ).tz("Asia/Hong_Kong").format("DD");
          HH   = dayjs( now ).tz("Asia/Hong_Kong").format("HH");
          mm   = dayjs( now ).tz("Asia/Hong_Kong").format("mm");
          ss   = dayjs( now ).tz("Asia/Hong_Kong").format("ss");
          sss  = dayjs( now ).tz("Asia/Hong_Kong").format("SSS");
          ms   = sss;
          
          // build from the above blocks
          date    = YYYY.concat( "-", MM, "-", DD );
          time    = HH.concat( "-", mm, "-", ss );
          time_ms = HH.concat( "-", mm, "-", ss, "-", ms );
          full    = date.concat( "_", time    );
          full_ms = date.concat( "_", time_ms );
  
  } catch (e) {
      console.log("timestamp.js : error in getting timestamp : ", e);
  }//try, catch

  const result_obj = {
    YY: YY,
    YYYY: YYYY,
    year: YYYY,
    month: MM,
    MM: MM,
    day: DD,
    DD: DD,
    hour: HH,
    HH: HH,
    min: mm,
    mm: mm,
    sec: ss,
    ss: ss,
    sss: sss,
    ms: ms,
    date: date,
    time: time,
    time_ms: time_ms,
    full: full,
    full_ms: full_ms,
    filename: full_ms
  }; // result_obj

  return result_obj;

}; // function dallmo_timestamp
const init = dallmo_timestamp;
///////////////////////////////////////////////////////////////////////
/*
const dallmo_timestamp = {
  init: init,
  refresh: refresh,
}; // dallmo_timestamp
*/
///////////////////////////////////////////////////////////////////////
// collect those to be exported here with an object

///////////////////////////////////////////////////////////////////////
// all exports go here
module.exports = dallmo_timestamp;
