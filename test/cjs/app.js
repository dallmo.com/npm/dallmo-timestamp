"use strict";

( async()=>{

  // using ESM-native module via dynamic import
  const dt = await import("dallmo-timestamp");
  
  console.log( dt.dallmo_timestamp().full_ms );
  console.log( dt.dallmo_timestamp().full_ms );
  console.log( dt.dallmo_timestamp().full_ms );

})(); // self-run async main
