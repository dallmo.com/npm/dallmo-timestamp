# dallmo-timestamp

- a simple wrapper based on dayjs
- ESM-native
- using this in CJS has to be done via [dynamic import][ref-2] 

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
[ref-2]: https://v8.dev/features/dynamic-import

# usage
## cjs
```
"use strict";

( async()=>{

  // using ESM-native module via dynamic import
  const dt = await import("dallmo-timestamp");
  
  console.log( dt.dallmo_timestamp().full_ms );
  console.log( dt.dallmo_timestamp().full_ms );
  console.log( dt.dallmo_timestamp().full_ms );

})(); // self-run async main
```

## esm
```
import {dallmo_timestamp} from 'dallmo-timestamp';

console.log( dallmo_timestamp().full_ms );
console.log( dallmo_timestamp().full_ms );
console.log( dallmo_timestamp().full_ms );
```

## the timestamp object properties
- YY
- YYYY
- year
- month
- MM
- day
- DD
- hour
- HH
- min
- mm
- sec
- ss
- sss
- ms
- date
- time
- time_ms
- full
- full_ms
- filename
