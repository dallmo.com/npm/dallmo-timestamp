"use strict";

// all are required for timezone adjustment below
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc.js';
import timezone from 'dayjs/plugin/timezone.js';

      dayjs.extend(utc);
      dayjs.extend(timezone);

/*
  ref : 
    display format : https://day.js.org/docs/en/display/format
*/

///////////////////////////////////////////////////////////////////////
function dallmo_timestamp(){
  
  let YY, YYYY, MM, DD, HH, mm, ss, sss, ms;
  let date, time, time_ms, full, full_ms;

  const timezone = "Asia/Hong_Kong";

  try {
  
    const now = new Date();
          YY   = dayjs( now ).tz(timezone).format("YY");
          YYYY = dayjs( now ).tz(timezone).format("YYYY");
          MM   = dayjs( now ).tz(timezone).format("MM");
          DD   = dayjs( now ).tz(timezone).format("DD");
          HH   = dayjs( now ).tz(timezone).format("HH");
          mm   = dayjs( now ).tz(timezone).format("mm");
          ss   = dayjs( now ).tz(timezone).format("ss");
          sss  = dayjs( now ).tz(timezone).format("SSS");
          ms   = sss;
          
          // build from the above blocks
          date    = YYYY.concat( "-", MM, "-", DD );
          time    = HH.concat( "-", mm, "-", ss );
          time_ms = HH.concat( "-", mm, "-", ss, "-", ms );
          full    = date.concat( "_", time    );
          full_ms = date.concat( "_", time_ms );
  
  } catch (error) {
      console.log("timestamp.js : error in getting timestamp : ", error.message);
  }//try, catch

  const result_obj = {
    YY: YY,
    YYYY: YYYY,
    year: YYYY,
    month: MM,
    MM: MM,
    day: DD,
    DD: DD,
    hour: HH,
    HH: HH,
    min: mm,
    mm: mm,
    sec: ss,
    ss: ss,
    sss: sss,
    ms: ms,
    date: date,
    time: time,
    time_ms: time_ms,
    full: full,
    full_ms: full_ms,
    filename: full_ms
  }; // result_obj

  return result_obj;

}; // function dallmo_timestamp
///////////////////////////////////////////////////////////////////////
// all exports go here
export {

  dallmo_timestamp,

};//
